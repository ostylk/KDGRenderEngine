package de.ostylk.kdgrender.shader;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.io.*;
import java.nio.FloatBuffer;

public abstract class Shader {

    private int programID;

    private static FloatBuffer buffer = BufferUtils.createFloatBuffer(16);

    public Shader(InputStream vertIS, InputStream fragIS) {
        int vertID = createShader(readFile(vertIS), GL20.GL_VERTEX_SHADER);
        int fragID = createShader(readFile(fragIS), GL20.GL_FRAGMENT_SHADER);
        programID = createProgram(vertID, fragID);
        bind();
        initAttributes();
        initUniformLocations();
        unbind();
    }

    public void bindAttribute(String name, int location) {
        GL20.glBindAttribLocation(programID, location, name);
    }

    public int getUniformLocation(String name) {
        return GL20.glGetUniformLocation(programID, name);
    }

    public void loadInt(int location, int value) {
        GL20.glUniform1i(location, value);
    }

    public void loadFloat(int location, float value) {
        GL20.glUniform1f(location, value);
    }

    public void loadVector2(int location, Vector2f value) {
        GL20.glUniform2f(location, value.x, value.y);
    }

    public void loadVector3(int location, Vector3f value) {
        GL20.glUniform3f(location, value.x, value.y, value.z);
    }

    public void loadMatrix4(int location, Matrix4f value) {
        value.store(buffer);
        buffer.flip();
        GL20.glUniformMatrix4(location, false, buffer);
    }

    public void bind() {
        GL20.glUseProgram(programID);
    }

    public void unbind() {
        GL20.glUseProgram(0);
    }

    public void cleanup() {
        unbind();
        GL20.glDeleteProgram(programID);
    }

    public abstract void initUniformLocations();
    public abstract void initAttributes();

    private String readFile(InputStream is) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8")); // Create the BufferedReader

            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {    // Read the shader source in, line by line
                builder.append(line).append("\n");  // Append it to the final string
            }

            return builder.toString();  // Return
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Shader source is not available");
    }

    private int createShader(String source, int type) {
        int shaderID = GL20.glCreateShader(type);
        GL20.glShaderSource(shaderID, source);
        GL20.glCompileShader(shaderID);

        String error = GL20.glGetShaderInfoLog(shaderID, 4096);
        if (!error.equals("")) {
            String shaderType = "";
            switch (type) {
                case GL20.GL_VERTEX_SHADER:
                    shaderType = "vertex shader";
                    break;
                case GL20.GL_FRAGMENT_SHADER:
                    shaderType = "fragment shader";
                    break;
            }
            System.err.println("Error: Couldn't compile " + shaderType);
            System.err.println(error);
            System.exit(-1);    // Turn of the program for safety.
        }

        return shaderID;
    }

    private int createProgram(int vertID, int fragID) {
        int programID = GL20.glCreateProgram();
        GL20.glAttachShader(programID, vertID);
        GL20.glAttachShader(programID, fragID);
        GL20.glLinkProgram(programID);
        GL20.glValidateProgram(programID);

        String error = GL20.glGetProgramInfoLog(programID, 4096);
        if (!error.equals("")) {
            System.err.println("Error: linking program");
            System.err.println(error);
            System.exit(-1);    // Turn off for safety
        }

        GL20.glDeleteShader(vertID);
        GL20.glDeleteShader(fragID);

        return programID;
    }
}
