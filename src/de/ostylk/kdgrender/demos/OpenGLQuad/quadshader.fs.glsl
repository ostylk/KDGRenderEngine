#version 330 core

layout (location = 0) out vec4 color;

void main() {
    // If you want to change the color of the quad
    // Try it here
                //R   G    B    A
    color = vec4(1.0, 1.0, 0.0, 1.0);
}
