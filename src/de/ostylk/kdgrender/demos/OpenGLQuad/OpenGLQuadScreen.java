package de.ostylk.kdgrender.demos.OpenGLQuad;

import de.ostylk.kdgrender.math.MathUtil;
import de.ostylk.kdgrender.model.Loader;
import de.ostylk.kdgrender.model.Mesh;
import de.ostylk.kdgrender.model.primitives.Quad;
import de.ostylk.kdgrender.render.BatchRenderer;
import de.ostylk.kdgrender.screen.Screen;
import de.ostylk.kdgrender.screen.ScreenManager;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

public class OpenGLQuadScreen implements Screen {

    private Mesh mesh;  // You want to create a mesh for your quad
    private OpenGLQuadShader shader;    // Also you want a shader which takes care of rendering your mesh

    @Override
    public void init(Loader loader) {

    }

    @Override
    public void enter(Loader loader) {
        // Now you have 2 possibilities
        boolean simple = false;
        if (simple) {   // You can use our predefined quad
            mesh = new Quad();
        } else {    // Or you can create your own quad or other geometric shapes
            // This list defines every point of your shape
            // You might notice that I'm writing 2 floats in one line
            // The coordinates are 2 dimensional vectors
            float[] vertices = {
                     0.5f,  0.5f,
                    -0.5f,  0.5f,
                    -0.5f, -0.5f,
                     0.5f, -0.5f
            };

            // Now the image the above as an array of 2 dimensional vectors
            // The first line will be 0
            // the second line 1
            // etc.
            // Now you need to tell OpenGL how you want to tie the points up
            // OpenGL "only" can render triangles
            // So you connect 3 points in clockwise order and create a triangles
            // Repeat this until you got your desired shape
            // Sketch this on a paper for better imagination
            int[] indices = {
                    0, 1, 2,
                    0, 2, 3
            };

            // Now you need to create the mesh
            // This function takes your data and uploads it into your GPU
            // and prepares it for rendering
            mesh = loader.getMeshLoader().loadMesh(vertices, indices);
        }

        shader = new OpenGLQuadShader();    // Initialize it
        shader.bind();
        // Here we load up the projection matrix
        shader.loadProjectionMatrix(MathUtil.getProjectionMatrix(Display.getWidth(), Display.getHeight()));
        shader.unbind();
    }

    @Override
    public void update(ScreenManager screenMgr, Loader loader, float delta) {

    }

    @Override
    public void render(ScreenManager screenMgr, Loader loader, BatchRenderer renderer) {
        // This should look familiar to you if you know OpenGL
        // Here we are settings the color
        // And clearing the screen with that color
        GL11.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);

        // shader.bind() binds the OpenGL shader(glUseProgram(id))
        shader.bind();
        // The mesh holds the VertexArrayObject of your mesh
        // This method binds it and sets up every thing for rendering
        mesh.bind();
        // This method should look familiar to you
        // It just renders the vao.
        GL11.glDrawElements(GL11.GL_TRIANGLES, mesh.getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
        mesh.unbind();
        shader.unbind();
    }

    @Override
    public void leave(Loader loader) {
        shader.cleanup(); // Delete your shader from memory when leaving screen
    }

    @Override
    public void exit(Loader loader) {

    }
}
