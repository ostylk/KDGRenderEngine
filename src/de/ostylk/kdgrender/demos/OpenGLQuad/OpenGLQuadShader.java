package de.ostylk.kdgrender.demos.OpenGLQuad;

import de.ostylk.kdgrender.shader.Shader;
import org.lwjgl.util.vector.Matrix4f;

public class OpenGLQuadShader extends Shader {

    private static final String VERT_PATH = "quadshader.vs.glsl";
    private static final String FRAG_PATH = "quadshader.fs.glsl";

    // The uniform location of the projection matrix
    private int location_projectionMatrix;

    public OpenGLQuadShader() {
        // The super-constructor loads the shader sources from an InputStream
        super(OpenGLQuadShader.class.getResourceAsStream(VERT_PATH), OpenGLQuadShader.class.getResourceAsStream(FRAG_PATH));
    }

    @Override
    public void initUniformLocations() {
        // With this method you get the location of an uniform
        location_projectionMatrix = getUniformLocation("projectionMatrix"); // you just need to pass its name
    }

    @Override
    public void initAttributes() {
        // This method binds the in variable with the name "position" onto location 0
        // It means it gets the data at location 0 in the vao
        bindAttribute("position", 0);
    }

    public void loadProjectionMatrix(Matrix4f projection) {
        // This function loads the matrix into the shader
        // We need this matrix to render everything without
        loadMatrix4(location_projectionMatrix, projection);
    }
}
