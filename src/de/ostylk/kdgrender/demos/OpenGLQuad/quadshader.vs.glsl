#version 330 core

layout (location = 0) in vec2 position;

// The projection matrix
uniform mat4 projectionMatrix;

void main() {
    // Just reading the passed data and
    // feeding it to OpenGL
    gl_Position = projectionMatrix * vec4(position, 0.5, 1.0);
}
