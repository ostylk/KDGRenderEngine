package de.ostylk.kdgrender.demos.CamDemo;

import de.ostylk.kdgrender.math.Camera;
import de.ostylk.kdgrender.model.Loader;
import de.ostylk.kdgrender.model.entities.Entity;
import de.ostylk.kdgrender.model.primitives.Quad;
import de.ostylk.kdgrender.render.BatchRenderer;
import de.ostylk.kdgrender.screen.Screen;
import de.ostylk.kdgrender.screen.ScreenManager;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CamDemoScreen implements Screen {

    private Camera cam;

    private List<Entity> entities;

    @Override
    public void init(Loader loader) {

    }

    @Override
    public void enter(Loader loader) {
        cam = new Camera(new Vector2f(0, 0));

        entities = new ArrayList<>();

        Quad quad = new Quad();
        Random r = new Random();

        for (int i = -10; i <= 10; i++) {
            for (int k = -10; k <= 10; k++) {
                Entity entity = new Entity(quad, new Vector2f(i, k), new Vector4f(Math.abs(r.nextFloat()), Math.abs(r.nextFloat()), Math.abs(r.nextFloat()), 1));
                entities.add(entity);
            }
        }
    }

    @Override
    public void update(ScreenManager screenMgr, Loader loader, float delta) {
        if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
            cam.add(-2 * delta, 0);
        } else if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            cam.add(2 * delta, 0);
        }

        if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
            cam.add(0, -2 * delta);
        } else if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
            cam.add(0, 2 * delta);
        }

        cam.zoom(Mouse.getDWheel() * -delta * 5f);
    }

    @Override
    public void render(ScreenManager screenMgr, Loader loader, BatchRenderer renderer) {
        for (Entity entity : entities) {
            renderer.submit(entity);
        }

        renderer.setCamera(cam);
        renderer.render(new Vector3f(0, 0, 0));
    }

    @Override
    public void leave(Loader loader) {

    }

    @Override
    public void exit(Loader loader) {

    }
}
