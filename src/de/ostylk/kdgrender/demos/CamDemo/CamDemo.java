package de.ostylk.kdgrender.demos.CamDemo;

import de.ostylk.kdgrender.Engine;
import de.ostylk.kdgrender.display.DisplaySettings;

public class CamDemo extends Engine {

    @Override
    public String initScreens() {
        initScreen("DemoScreen", new CamDemoScreen());
        return "DemoScreen";
    }

    public static void main(String[] args) {
        DisplaySettings settings = new DisplaySettings(1280, 720);
        settings.title = "CamDemo";
        settings.vsync = false;
        settings.showFPS = true;

        CamDemo demo = new CamDemo();
        demo.init(settings);
    }
}
