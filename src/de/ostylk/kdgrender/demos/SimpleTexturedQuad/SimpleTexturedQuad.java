package de.ostylk.kdgrender.demos.SimpleTexturedQuad;

import de.ostylk.kdgrender.Engine;
import de.ostylk.kdgrender.display.DisplaySettings;

public class SimpleTexturedQuad extends Engine {

    @Override
    public String initScreens() {
        initScreen("Screen1", new SimpleTexturedQuadScreen());
        return "Screen1";
    }

    public static void main(String[] args) {
        DisplaySettings settings = new DisplaySettings(1280, 720);
        settings.title = "SimpleTexturedQuad";
        settings.vsync = false;
        settings.showFPS = true;

        SimpleTexturedQuad quad = new SimpleTexturedQuad();
        quad.init(settings);
    }
}
