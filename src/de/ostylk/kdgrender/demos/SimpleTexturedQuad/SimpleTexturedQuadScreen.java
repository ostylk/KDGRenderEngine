package de.ostylk.kdgrender.demos.SimpleTexturedQuad;

import de.ostylk.kdgrender.model.Loader;
import de.ostylk.kdgrender.model.entities.Entity;
import de.ostylk.kdgrender.model.primitives.Quad;
import de.ostylk.kdgrender.render.BatchRenderer;
import de.ostylk.kdgrender.screen.Screen;
import de.ostylk.kdgrender.screen.ScreenManager;
import de.ostylk.kdgrender.texture.Texture;
import de.ostylk.kdgrender.texture.TextureLoader;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class SimpleTexturedQuadScreen implements Screen {

    private Entity entity;

    @Override
    public void init(Loader loader) {

    }

    @Override
    public void enter(Loader loader) {
        Quad quad = new Quad();
        // Load the texture with the textureloader
        // First argument is the InputStream to the file
        // Second argument is the Type of image (PNG, JPG)
        Texture texture = loader.getTextureLoader().loadTexture(SimpleTexturedQuadScreen.class.getResourceAsStream("box.png"), TextureLoader.Type.PNG);

        // Look up in the SimpleQuad demo.
        // The only difference is that we pass a texture object instead a color
        entity = new Entity(quad, texture, new Vector2f(0, 0));
    }

    @Override
    public void update(ScreenManager screenMgr, Loader loader, float delta) {

    }

    @Override
    public void render(ScreenManager screenMgr, Loader loader, BatchRenderer renderer) {
        // Look up in the demo SimpleQuad
        renderer.submit(entity);

        renderer.render(new Vector3f(1, 1, 1));
    }

    @Override
    public void leave(Loader loader) {

    }

    @Override
    public void exit(Loader loader) {

    }
}
