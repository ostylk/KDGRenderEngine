package de.ostylk.kdgrender.demos.SimpleQuad;

import de.ostylk.kdgrender.model.Loader;
import de.ostylk.kdgrender.model.entities.Entity;
import de.ostylk.kdgrender.model.primitives.Quad;
import de.ostylk.kdgrender.render.BatchRenderer;
import de.ostylk.kdgrender.screen.Screen;
import de.ostylk.kdgrender.screen.ScreenManager;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

public class SimpleQuadScreen implements Screen {

    private Entity entity;

    @Override
    public void init(Loader loader) {

    }

    @Override
    public void enter(Loader loader) {
        // Create the type of object you want (we want a quad)
        Quad quad = new Quad();
        // Create an entity with the given geometric shape
        // The second argument is the position of the quad ((0|0) being the middle)
        // The third argument is the color of the entity in RGBA format
        //                                                         R  G  B  A
        entity = new Entity(quad, new Vector2f(0, 0), new Vector4f(1, 1, 1, 1));
    }

    @Override
    public void update(ScreenManager screenMgr, Loader loader, float delta) {
    }

    @Override
    public void render(ScreenManager screenMgr, Loader loader, BatchRenderer renderer) {
        // You have to submit all entities you want to render
        renderer.submit(entity);

        // Render EVERYTHING!
        // The argument is the "clear color" in RGB format
        renderer.render(new Vector3f(0, 0, 0));
    }

    @Override
    public void leave(Loader loader) {

    }

    @Override
    public void exit(Loader loader) {

    }
}
