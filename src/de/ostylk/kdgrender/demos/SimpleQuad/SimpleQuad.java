package de.ostylk.kdgrender.demos.SimpleQuad;

import de.ostylk.kdgrender.Engine;
import de.ostylk.kdgrender.display.DisplaySettings;

public class SimpleQuad extends Engine {

    @Override
    public String initScreens() {
        initScreen("Screen1", new SimpleQuadScreen());
        return "Screen1";
    }

    public static void main(String[] args) {
        DisplaySettings settings = new DisplaySettings(1280, 720);

        settings.title = "SimpleQuad";
        settings.showFPS = true;
        settings.vsync = false;

        SimpleQuad quad = new SimpleQuad();
        quad.init(settings);
    }
}
