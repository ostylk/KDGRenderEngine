package de.ostylk.kdgrender.demos.OpenGLTexturedQuad;

import de.ostylk.kdgrender.shader.Shader;
import org.lwjgl.util.vector.Matrix4f;

public class OpenGLTexturedQuadShader extends Shader {

    private static final String VERT_PATH = "quadshader.vs.glsl";
    private static final String FRAG_PATH = "quadshader.fs.glsl";

    // The uniform location of the projection matrix
    private int location_projectionMatrix;

    // The uniform location of the texture sampler
    private int location_textureSampler;

    public OpenGLTexturedQuadShader() {
        // The super-constructor loads the shader sources from an InputStream
        super(OpenGLTexturedQuadShader.class.getResourceAsStream(VERT_PATH), OpenGLTexturedQuadShader.class.getResourceAsStream(FRAG_PATH));
    }

    @Override
    public void initUniformLocations() {
        // With this method you get the location of an uniform
        location_projectionMatrix = getUniformLocation("projectionMatrix"); // you just need to pass its name

        location_textureSampler = getUniformLocation("textureSampler");
    }

    @Override
    public void initAttributes() {
        // This method binds the in variable with the name "position" onto location 0
        // It means it gets the data at location 0 in the vao
        bindAttribute("position", 0);
        bindAttribute("texcoord", 1);
    }

    public void connectTextureUnits() {
        // Connect texture sampler to texture unit 0
        loadInt(location_textureSampler, 0);
    }

    public void loadProjectionMatrix(Matrix4f projection) {
        // This function loads the matrix into the shader
        // We need this matrix to render everything without
        loadMatrix4(location_projectionMatrix, projection);
    }
}
