#version 330 core

layout (location = 0) out vec4 color;

in vec2 pass_texcoord;

uniform sampler2D textureSampler;

void main() {
    color = texture(textureSampler, pass_texcoord); // Sampling the texture at the texture coordinates
}
