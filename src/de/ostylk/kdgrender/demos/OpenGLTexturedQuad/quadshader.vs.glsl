#version 330 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texcoord;

out vec2 pass_texcoord;

// The projection matrix
uniform mat4 projectionMatrix;

void main() {
    pass_texcoord = texcoord;   // Passing the texture coordinates to the next shader stage (fragment shader)
    // Just reading the passed data and
    // feeding it to OpenGL
    gl_Position = projectionMatrix * vec4(position, 0.5, 1.0);
}
