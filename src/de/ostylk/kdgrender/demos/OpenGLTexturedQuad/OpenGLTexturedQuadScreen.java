package de.ostylk.kdgrender.demos.OpenGLTexturedQuad;

import de.ostylk.kdgrender.math.MathUtil;
import de.ostylk.kdgrender.model.Loader;
import de.ostylk.kdgrender.model.Mesh;
import de.ostylk.kdgrender.model.primitives.Quad;
import de.ostylk.kdgrender.render.BatchRenderer;
import de.ostylk.kdgrender.screen.Screen;
import de.ostylk.kdgrender.screen.ScreenManager;
import de.ostylk.kdgrender.texture.Texture;
import de.ostylk.kdgrender.texture.TextureLoader;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

public class OpenGLTexturedQuadScreen implements Screen {

    private Mesh mesh;  // You want to create a mesh for your quad
    private Texture texture;
    private OpenGLTexturedQuadShader shader;    // Also you want a shader which takes care of rendering your mesh

    @Override
    public void init(Loader loader) {

    }

    @Override
    public void enter(Loader loader) {
        // Now you have 2 possibilities
        boolean simple = true;
        if (simple) {   // You can use our predefined quad
            mesh = new Quad();
        } else {    // Or you can create your own quad or other geometric shapes
            // The same as in the demo "OpenGLQuad" look there for explanation
            float[] vertices = {
                     0.5f,  0.5f,
                    -0.5f,  0.5f,
                    -0.5f, -0.5f,
                     0.5f, -0.5f
            };

            // Every point you've defined above gets here an texture coordinate
            float[] texcoords = {
                    1f, 0f,
                    0f, 0f,
                    0f, 1f,
                    1f, 1f
            };

            int[] indices = {
                    0, 1, 2,
                    0, 2, 3
            };

            // Now you need to create the mesh
            // This function takes your data and uploads it into your GPU
            // and prepares it for rendering
            mesh = loader.getMeshLoader().loadMesh(vertices, texcoords, indices);
        }

        shader = new OpenGLTexturedQuadShader();    // Initialize it
        shader.bind();
        // Here we load up the projection matrix
        shader.loadProjectionMatrix(MathUtil.getProjectionMatrix(Display.getWidth(), Display.getHeight()));
        // Here we connect the texture units
        shader.connectTextureUnits();
        shader.unbind();

        // Loading the Texture
        // The loadTexture method expects an InputStream
        texture = loader.getTextureLoader().loadTexture(OpenGLTexturedQuadScreen.class.getResourceAsStream("box.png"), TextureLoader.Type.PNG);
    }

    @Override
    public void update(ScreenManager screenMgr, Loader loader, float delta) {

    }

    @Override
    public void render(ScreenManager screenMgr, Loader loader, BatchRenderer renderer) {
        // This should look familiar to you if you know OpenGL
        // Here we are settings the color
        // And clearing the screen with that color
        GL11.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);

        // shader.bind() binds the OpenGL shader(glUseProgram(id))
        shader.bind();
        // The mesh holds the VertexArrayObject of your mesh
        // This method binds it and sets up every thing for rendering
        mesh.bind();
        // Also bind the texture
        texture.bindTexture(0);
        // This method should look familiar to you
        // It just renders the vao.
        GL11.glDrawElements(GL11.GL_TRIANGLES, mesh.getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
        mesh.unbind();
        shader.unbind();
    }

    @Override
    public void leave(Loader loader) {
        shader.cleanup(); // Delete your shader from memory when leaving screen
    }

    @Override
    public void exit(Loader loader) {

    }
}
