package de.ostylk.kdgrender.demos.OpenGLTexturedQuad;

import de.ostylk.kdgrender.Engine;
import de.ostylk.kdgrender.display.DisplaySettings;

public class OpenGLTexturedQuad extends Engine {

    /*
    If you want to create a game without learning OpenGL, take a look at the "SimpleQuad" and "TexturedQuad" demo
    This shows how you could create your own meshes with our MeshLoader and render it with OpenGL
     */

    @Override
    public String initScreens() {
        initScreen("Screen1", new OpenGLTexturedQuadScreen());
        return "Screen1";
    }

    public static void main(String[] args) {
        DisplaySettings settings = new DisplaySettings(1280, 720);

        settings.title = "OpenGLTexturedQuad";
        settings.showFPS = true;
        settings.vsync = false;

        OpenGLTexturedQuad demo = new OpenGLTexturedQuad();
        demo.init(settings);
    }
}
