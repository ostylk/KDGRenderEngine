package de.ostylk.kdgrender.demos.SimpleWindow;

import de.ostylk.kdgrender.Engine;
import de.ostylk.kdgrender.display.DisplaySettings;

public class SimpleWindow extends Engine {

    @Override
    public String initScreens() {
        initScreen("Screen1", new SimpleWindowScreen());    // Initialize a screen with a specific name
        return "Screen1";   // Return the name of the first screen you want to show
    }

    public static void main(String[] args) {
        DisplaySettings settings = new DisplaySettings(1280, 720); // Indicates how big the display should be.

        settings.title = "SimpleWindow"; // Sets the window's title
        settings.showFPS = true;         // shows fps. Great for performance tuning
        settings.vsync = false;          // If FPS should be capped to monitor refresh rate
        settings.fullscreen = false;     // enables fullscreen (DisplayMode must be available);

        SimpleWindow simple = new SimpleWindow();   // Create new instance of our selves
        simple.init(settings);                      // Initialize OpenGL Context and Window
    }
}
