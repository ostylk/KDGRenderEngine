package de.ostylk.kdgrender.demos.SimpleWindow;

import de.ostylk.kdgrender.model.Loader;
import de.ostylk.kdgrender.render.BatchRenderer;
import de.ostylk.kdgrender.screen.Screen;
import de.ostylk.kdgrender.screen.ScreenManager;

public class SimpleWindowScreen implements Screen {

    @Override
    public void init(Loader loader) {
        // This function gets called once the program starts
    }

    @Override
    public void enter(Loader loader) {
        // This function gets called whenever the programs enters this screen
    }

    @Override
    public void update(ScreenManager screenMgr, Loader loader, float delta) {
        // This function gets called every frame/tick
        // delta is the time difference between frames
    }

    @Override
    public void render(ScreenManager screenMgr, Loader loader, BatchRenderer renderer) {
        // This function gets called every frame/tick
        // The BatchRenderer is for rendering
    }

    @Override
    public void leave(Loader loader) {
        // This function gets called whenever the program leaves this screen
    }

    @Override
    public void exit(Loader loader) {
        // This function gets called when the program exited
    }
}
