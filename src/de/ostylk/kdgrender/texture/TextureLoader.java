package de.ostylk.kdgrender.texture;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL30;

import java.io.IOException;
import java.io.InputStream;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

public class TextureLoader {

    public static Texture WHITE_TEXTURE;

    private List<Integer> textures;
    private float lod_bias;

    public enum Type {
        PNG, JPG;

        @Override
        public String toString() {
            switch (this) {
                case PNG:
                    return "PNG";
                case JPG:
                    return "JPG";
                default:
                    throw new IllegalStateException("Impossible!");
            }
        }
    }

    public TextureLoader(float lod_bias) {
        textures = new ArrayList<>();

        this.lod_bias = lod_bias;

        if (WHITE_TEXTURE == null) {
            WHITE_TEXTURE = loadTexture(TextureLoader.class.getResourceAsStream("default_tex.png"), Type.PNG);
        }
    }

    public Texture loadTexture(InputStream is, Type type) {
        org.newdawn.slick.opengl.Texture texture = null;
        try {
            texture = org.newdawn.slick.opengl.TextureLoader.getTexture(type.toString(), is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
        GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL14.GL_TEXTURE_LOD_BIAS, lod_bias);
        int textureID = texture.getTextureID();
        textures.add(textureID);
        return new Texture((int) texture.getWidth(), (int) texture.getHeight(), textureID);
    }

    public Texture createTexture(int width, int height, int type, int format, int filter) {
        int texID = GL11.glGenTextures();

        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texID);
        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, format, width, height, 0, type, GL11.GL_UNSIGNED_INT, (IntBuffer) null);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, filter);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, filter);
        textures.add(texID);

        return new Texture(width, height, texID);
    }

    public void cleanup() {
        for (int texture : textures) {
            GL11.glDeleteTextures(texture);
        }
    }
}
