package de.ostylk.kdgrender.texture;

import de.ostylk.kdgrender.model.Loader;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import java.nio.IntBuffer;

public class DeferredFramebuffer extends Framebuffer {

    private int depthBuffer;

    private Texture colorTexture;
    private Texture posTexture;

    public DeferredFramebuffer(Loader loader, int width, int height) {
        super(loader, width, height);
    }

    @Override
    public void setup(Loader loader) {
        colorTexture = loader.getTextureLoader().createTexture(getWidth(), getHeight(), GL11.GL_RGB, GL30.GL_RGB32F, GL11.GL_NEAREST);
        posTexture = loader.getTextureLoader().createTexture(getWidth(), getHeight(), GL11.GL_RGB, GL30.GL_RGB32F, GL11.GL_NEAREST);

        depthBuffer = GL30.glGenRenderbuffers();
        GL30.glBindRenderbuffer(GL30.GL_RENDERBUFFER, depthBuffer);
        GL30.glRenderbufferStorage(GL30.GL_RENDERBUFFER, GL11.GL_DEPTH_COMPONENT, getWidth(), getHeight());
    }

    @Override
    public void attachTextures() {
        GL30.glFramebufferRenderbuffer(GL30.GL_FRAMEBUFFER, GL30.GL_DEPTH_ATTACHMENT, GL30.GL_RENDERBUFFER, depthBuffer);
        GL30.glFramebufferTexture2D(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT0, GL11.GL_TEXTURE_2D, colorTexture.getTexID(), 0);
        GL30.glFramebufferTexture2D(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT1, GL11.GL_TEXTURE_2D, posTexture.getTexID(), 0);
    }

    @Override
    public void setupDrawbuffers() {
        // Describe OpenGL how to render stuff
        IntBuffer drawBuffer = BufferUtils.createIntBuffer(2);
        int draws[] = { GL30.GL_COLOR_ATTACHMENT0, GL30.GL_COLOR_ATTACHMENT1 };
        drawBuffer.put(draws);
        drawBuffer.flip();
        GL20.glDrawBuffers(drawBuffer);
    }

    public void bindTexture(int n) {
        GL13.glActiveTexture(GL13.GL_TEXTURE0 + n);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, n == 0 ? colorTexture.getTexID() : posTexture.getTexID());
    }

    public void bindTextures() {
        bindTexture(0);
        bindTexture(1);
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
    }

    public void unbindTextures() {
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
    }
}
