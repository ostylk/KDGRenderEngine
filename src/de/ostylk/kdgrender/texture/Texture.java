package de.ostylk.kdgrender.texture;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

public class Texture {

    private int width, height;
    private int texID;

    protected Texture(int width, int height, int texID) {
        this.width = width;
        this.height = height;
        this.texID = texID;
    }

    public void bind() {
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texID);
    }

    public void unbind() {
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
    }

    public void bindTexture(int textureUnit) {
        GL13.glActiveTexture(GL13.GL_TEXTURE0 + textureUnit);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texID);
    }

    public int getWidth() { return width; }
    public int getHeight() { return height; }
    public int getTexID() { return texID; }
}
