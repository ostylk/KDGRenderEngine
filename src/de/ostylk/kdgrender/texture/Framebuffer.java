package de.ostylk.kdgrender.texture;

import de.ostylk.kdgrender.model.Loader;
import de.ostylk.kdgrender.render.BatchRenderer;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;

public abstract class Framebuffer {

    private int fboID;
    private int width, height;

    public Framebuffer(Loader loader, int width, int height) {
        this.width = width;
        this.height = height;

        // Create and bind the framebuffer
        fboID = GL30.glGenFramebuffers();
        GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, fboID);

        setup(loader);

        attachTextures();
        setupDrawbuffers();

        GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
    }

    public void bind() {
        GL11.glViewport(0, 0, width, height);
        GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, fboID);
    }

    public void unbind(BatchRenderer renderer) {
        GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
        renderer.defaultViewport();
    }

    public int getFboID() { return fboID; }
    public int getWidth() { return width; }
    public int getHeight() { return height; }

    public abstract void setup(Loader loader);
    public abstract void attachTextures();
    public abstract void setupDrawbuffers();
}
