package de.ostylk.kdgrender.screen;

import de.ostylk.kdgrender.model.Loader;
import org.lwjgl.opengl.Display;

import java.util.HashMap;

public class ScreenManager {

    private HashMap<String, Screen> screens;
    private Screen current;

    private Loader loader;

    private boolean isCloseRequested = false;

    public ScreenManager(Loader loader) {
        screens = new HashMap<>();

        this.loader = loader;
    }

    public void addScreen(String name, Screen screen) {
        screens.put(name, screen);
    }

    public void finalizeScreens() {
        for (Screen screen : screens.values()) {
            screen.init(loader);
        }
    }

    public void cleanupScreens() {
        for (Screen screen : screens.values()) {
            screen.exit(loader);
        }
    }

    public void requestClose() {
        isCloseRequested = true;
    }

    public void startScreen(String name) {
        Screen next = screens.get(name);
        if (next == null) throw new RuntimeException("Screen " + name + " does not exist!");

        if (current != null) {
            current.leave(loader);
        }

        current = next;
        current.enter(loader);
    }

    public Screen getCurrent() { return current; }
    public boolean isCloseRequested() { return isCloseRequested || Display.isCloseRequested(); }
}
