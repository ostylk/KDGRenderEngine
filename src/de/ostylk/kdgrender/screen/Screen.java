package de.ostylk.kdgrender.screen;

import de.ostylk.kdgrender.model.Loader;
import de.ostylk.kdgrender.render.BatchRenderer;

public interface Screen {

    /**
     * This method gets called once the program starts
     */
    void init(Loader loader);

    /**
     * This method gets called whenever this screen gets current.
     */
    void enter(Loader loader);   // Gets called when the screen is entered

    /**
     * @param screenMgr ScreenManager for managing and switching screens
     * @param delta Time difference between frames in seconds
     *
     * This method gets called every frame
     */
    void update(ScreenManager screenMgr, Loader loader, float delta);               // Gets every tick called

    /**
     *
     * @param screenMgr ScreenManager for managing and switching screens
     * @param renderer A BatchRenderer used for drawing
     *
     * This method gets called every frame
     */
    void render(ScreenManager screenMgr, Loader loader, BatchRenderer renderer);    // Gets every tick called

    /**
     * This method gets called whenever the program leaves this screen(another screen gets current)
     */
    void leave(Loader loader);   // Gets called when the screen is being left

    /**
     * This method gets called when the program is being exited
     */
    void exit(Loader loader);
}
