package de.ostylk.kdgrender.model;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

public class MeshLoader {

    private List<Integer> vaos;

    public MeshLoader() {
        vaos = new ArrayList<>();
    }

    /**
     * @param vertices The vertices for the mesh. They are vec2!
     * @param indices The indices which connect up vertices..
     * @return Shape for the vertices passed
     */
    public Shape loadMesh(float[] vertices, int[] indices) {
        int vaoID = createVAO();
        bindElementBuffer(indices);
        storeDataInVAO(vertices, 0, 2);
        GL30.glBindVertexArray(0);
        return new Shape(vaoID, indices.length);
    }

    /**
     * @param vertices The vertices for the mesh. They are vec2!
     * @param texcoords The texcoords for the mesh. They are vec2!
     * @param indices The indices which connect up vertices..
     * @return Shape for the vertices passed
     */
    public Shape loadMesh(float[] vertices, float[] texcoords, int[] indices) {
        int vaoID = createVAO();
        bindElementBuffer(indices);
        storeDataInVAO(vertices, 0, 2);
        storeDataInVAO(texcoords, 1, 2);
        GL30.glBindVertexArray(0);
        return new Shape(vaoID, indices.length);
    }

    private int createVAO() {
        int vaoID = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vaoID);
        vaos.add(vaoID);
        return vaoID;
    }

    private void bindElementBuffer(int[] indices) {
        int vboID = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboID);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, storeIntData(indices), GL15.GL_STATIC_DRAW);
    }

    private void storeDataInVAO(float[] data, int location, int size) {
        int vboID = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, storeFloatData(data), GL15.GL_STATIC_DRAW); // Copy data into buffer
        GL20.glVertexAttribPointer(location, size, GL11.GL_FLOAT, false, 0, 0); // Select where to store in VAO
        GL20.glEnableVertexAttribArray(location);
    }

    private IntBuffer storeIntData(int[] data) {
        IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
        buffer.put(data);
        buffer.flip();
        return buffer;
    }

    private FloatBuffer storeFloatData(float[] data) {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
        buffer.put(data);
        buffer.flip();
        return buffer;
    }

    public void cleanup() {
        for (int vao : vaos) {
            GL30.glDeleteVertexArrays(vao);
        }
    }
}
