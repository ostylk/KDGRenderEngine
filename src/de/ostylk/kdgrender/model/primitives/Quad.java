package de.ostylk.kdgrender.model.primitives;

import de.ostylk.kdgrender.model.Loader;
import de.ostylk.kdgrender.model.Mesh;
import de.ostylk.kdgrender.model.Shape;

public class Quad implements Mesh {

    private static Shape shape;

    public static void init(Loader loader) {
        if (shape != null) return;

        float[] vertices = {
                0.5f,  0.5f,
                -0.5f,  0.5f,
                -0.5f, -0.5f,
                0.5f, -0.5f
        };

        float[] texcoords = {
                1f, 0f,
                0f, 0f,
                0f, 1f,
                1f, 1f
        };

        int[] indices = {
                0, 1, 2,
                0, 2, 3
        };

        shape = loader.getMeshLoader().loadMesh(vertices, texcoords, indices);
    }

    @Override
    public void bind() {
        shape.bind();
    }

    @Override
    public void unbind() {
        shape.unbind();
    }

    @Override
    public int getVertexCount() {
        return shape.getVertexCount();
    }
}
