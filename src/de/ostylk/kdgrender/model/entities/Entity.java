package de.ostylk.kdgrender.model.entities;

import de.ostylk.kdgrender.model.Mesh;
import de.ostylk.kdgrender.texture.Texture;
import de.ostylk.kdgrender.texture.TextureLoader;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

public class Entity {

    private Mesh mesh;
    private Texture texture;

    private Vector2f position;
    private float rotation;
    private float scale;

    private Vector4f color;

    public Entity(Mesh mesh, Vector2f position, Vector4f color) {
        this.mesh = mesh;
        this.texture = TextureLoader.WHITE_TEXTURE;

        this.position = position;
        this.rotation = 0;
        this.scale = 1;

        this.color = color;
    }

    public Entity(Mesh mesh, Texture texture, Vector2f position) {
        this.mesh = mesh;
        this.texture = texture;

        this.position = position;
        this.rotation = 0;
        this.scale = 1;

        this.color = new Vector4f(1, 1, 1, 1);
    }

    public Vector2f getPosition() {
        return position;
    }

    public void setPosition(Vector2f position) {
        this.position = position;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public Vector4f getColor() {
        return color;
    }

    public void setColor(Vector4f color) {
        this.color = color;
    }

    public Mesh getMesh() {
        return mesh;
    }

    public Texture getTexture() {
        return texture;
    }
}
