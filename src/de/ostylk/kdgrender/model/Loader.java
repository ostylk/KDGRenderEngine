package de.ostylk.kdgrender.model;

import de.ostylk.kdgrender.model.primitives.Quad;
import de.ostylk.kdgrender.texture.TextureLoader;

public class Loader {

    private MeshLoader meshLoader;
    private TextureLoader textureLoader;

    public Loader(MeshLoader meshLoader, TextureLoader textureLoader) {
        this.meshLoader = meshLoader;
        this.textureLoader = textureLoader;

        // Initialize every primitive
        Quad.init(this);
    }

    public MeshLoader getMeshLoader() { return meshLoader; }
    public TextureLoader getTextureLoader() { return textureLoader; }
}
