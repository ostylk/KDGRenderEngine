package de.ostylk.kdgrender.model;

public interface Mesh {

    void bind();
    void unbind();
    int getVertexCount();
}
