package de.ostylk.kdgrender.model;

import org.lwjgl.opengl.GL30;

public class Shape implements Mesh {

    private int vaoID;
    private int vertexCount;

    protected Shape(int vaoID, int vertexCount) {
        this.vaoID = vaoID;
        this.vertexCount = vertexCount;
    }

    public void bind() {
        GL30.glBindVertexArray(vaoID);
    }

    public void unbind() {
        GL30.glBindVertexArray(0);
    }

    public int getVertexCount() { return vertexCount; }
}
