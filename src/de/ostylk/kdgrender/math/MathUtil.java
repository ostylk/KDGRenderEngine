package de.ostylk.kdgrender.math;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class MathUtil {

    public static Matrix4f createTransformation(Vector2f translation, float rotation, float scale) {
        Matrix4f result = new Matrix4f();
        result.setIdentity();

        Matrix4f.translate(translation, result, result);
        Matrix4f.rotate((float) Math.toRadians(rotation), new Vector3f(0, 0, 1), result, result);
        Matrix4f.scale(new Vector3f(scale, scale, 1), result, result);

        return result;
    }

    public static Matrix4f createViewTransformation(Vector2f translation, float rotation, float zoom) {
        Matrix4f result = new Matrix4f();
        result.setIdentity();

        translation.negate(translation);

        Matrix4f.translate(translation, result, result);
        Matrix4f.rotate((float) Math.toRadians(rotation), new Vector3f(0, 0, 1), result, result);
        Matrix4f.scale(new Vector3f(zoom, zoom, 1), result, result);

        return result;
    }

    public static Matrix4f getProjectionMatrix(int width, int height) {
        Matrix4f result = new Matrix4f();
        result.setIdentity();

        float ratio = (float) (height) / (float) (width);
        Matrix4f.scale(new Vector3f(ratio, 1, 1), result, result);

        return result;
    }
}
