package de.ostylk.kdgrender.math;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class Camera {

    private Vector2f position;
    private float depth;

    public Camera(Vector2f position) {
        this.position = position;
        this.depth = 1;
    }

    public void add(float dx, float dy) {
        position.x += dx;
        position.y += dy;
    }

    public void zoom(float dz) {
        depth += dz;
    }

    public void setPosition(Vector2f position) { this.position = position; }
    public void setZoom(float depth) { this.depth = depth; }

    public Vector2f getPosition() { return position; }
    public float getZoom() { return depth; }

    public Matrix4f getViewMatrix() {
        Matrix4f viewMatrix = new Matrix4f();
        viewMatrix.setIdentity();

        // Create the illusion of the zooming out
        Matrix4f.scale(new Vector3f(1 / depth, 1 / depth, 1f), viewMatrix, viewMatrix);

        // Create the illusion of the camera moving
        // Actually the hole scene is being moved in the opposite direction of the camera
        Matrix4f.translate(new Vector2f(-position.x, -position.y), viewMatrix, viewMatrix);

        return viewMatrix;
    }
}
