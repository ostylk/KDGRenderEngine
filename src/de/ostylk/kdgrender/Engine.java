package de.ostylk.kdgrender;

import de.ostylk.kdgrender.display.DisplayManager;
import de.ostylk.kdgrender.display.DisplaySettings;
import de.ostylk.kdgrender.model.Loader;
import de.ostylk.kdgrender.model.MeshLoader;
import de.ostylk.kdgrender.render.BatchRenderer;
import de.ostylk.kdgrender.screen.Screen;
import de.ostylk.kdgrender.screen.ScreenManager;
import de.ostylk.kdgrender.texture.TextureLoader;

public abstract class Engine implements Runnable {


    private ScreenManager screenMgr;
    private DisplaySettings settings;
    private Loader loader;
    private BatchRenderer batchRenderer;

    private String firstScreen;

    private Thread thread;

    public Engine() {}

    public abstract String initScreens();

    public void initScreen(String name, Screen screen) {
        screenMgr.addScreen(name, screen);
    }

    private void prepareEngine() {
        // Initialize OpenGL context and create display
        DisplayManager.initDisplay(settings);

        // Create loaders and hook them up into one object
        MeshLoader meshLoader = new MeshLoader();
        TextureLoader textureLoader = new TextureLoader(-0.4f);
        loader = new Loader(meshLoader, textureLoader);

        batchRenderer = new BatchRenderer(loader);

        // Initialize our screen manager
        screenMgr = new ScreenManager(loader);
        // Initialize every screen and start the first one
        this.firstScreen = initScreens();
        screenMgr.finalizeScreens();
        screenMgr.startScreen(firstScreen);
    }

    public void run() {
        // Start initialization of our engine
        prepareEngine();

        while (!screenMgr.isCloseRequested()) {
            float delta = DisplayManager.getDelta();

            // Get current screen
            Screen current = screenMgr.getCurrent();

            // Call update and render methods
            current.update(screenMgr, loader, delta);
            //TODO: pass the BatchRenderer for rendering
            current.render(screenMgr, loader, batchRenderer);

            // Update Display
            DisplayManager.updateDisplay();
        }
        // Cleanup everything
        cleanupEngine();
    }

    private void cleanupEngine() {
        // Call every exit() method of the screens
        screenMgr.cleanupScreens();
        // Cleanup everything
        loader.getMeshLoader().cleanup();
        loader.getTextureLoader().cleanup();

        // Close the display
        DisplayManager.closeDisplay();
    }

    /**
     * @param settings Initial DisplaySettings
     * Initializes rendering engine with passed DisplaySettings
     */
    public void init(DisplaySettings settings) {
        this.settings = settings;

        thread = new Thread(this, "KDGRenderEngine");
        thread.start();
    }
}
