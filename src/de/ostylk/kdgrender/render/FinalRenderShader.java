package de.ostylk.kdgrender.render;

import de.ostylk.kdgrender.shader.Shader;

public class FinalRenderShader extends Shader {

    private int location_textureSampler;

    public FinalRenderShader() {
        super(FinalRenderShader.class.getResourceAsStream("finalrender.vs.glsl"), FinalRenderShader.class.getResourceAsStream("finalrender.fs.glsl"));
    }

    @Override
    public void initUniformLocations() {
        location_textureSampler = getUniformLocation("textureSampler");
    }

    @Override
    public void initAttributes() {
        bindAttribute("vertex", 0);
        bindAttribute("texcoord", 1);
    }

    public void connectTextureUnits() {
        loadInt(location_textureSampler, 0);
    }
}
