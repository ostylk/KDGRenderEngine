package de.ostylk.kdgrender.render;

import de.ostylk.kdgrender.model.entities.Entity;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL33;

import java.nio.FloatBuffer;
import java.util.List;

public class BatchBuffer {

    public static final int FLOAT_SIZE = 4;
    public static final int BATCH_SIZE = 8 * FLOAT_SIZE;    // 8 floats with 4 bytes
    public static final int MAX_BATCH_COUNT = 32768;
    public static final int BUFFER_SIZE = BATCH_SIZE * MAX_BATCH_COUNT;

    private int bufferPos;
    private FloatBuffer floatBufferPos;
    private int bufferColor;
    private FloatBuffer floatBufferColor;

    public BatchBuffer(int size) {
        createBuffer(size);

        floatBufferPos = BufferUtils.createFloatBuffer(size / FLOAT_SIZE / 2);
        floatBufferColor = BufferUtils.createFloatBuffer(size / FLOAT_SIZE / 2);
    }

    public void setupBuffer() {
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, bufferPos);
        GL20.glEnableVertexAttribArray(2);
        GL20.glVertexAttribPointer(2, 4, GL11.GL_FLOAT, false, 0, 0);
        GL33.glVertexAttribDivisor(2, 1);

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, bufferColor);
        GL20.glEnableVertexAttribArray(3);
        GL20.glVertexAttribPointer(3, 4, GL11.GL_FLOAT, false, 0, 0);
        GL33.glVertexAttribDivisor(3, 1);
    }

    private void createBuffer(int size) {
        bufferPos = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, bufferPos);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, size / 2, GL15.GL_DYNAMIC_DRAW);    // Buffer will be updated often and used often for drawing

        bufferColor = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, bufferColor);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, size / 2, GL15.GL_DYNAMIC_DRAW);    // Buffer will be updated often and used often for drawing
    }

    public void switchBuffer(List<Entity> entities) {
        float[] bufferPos = new float[4 * entities.size()];
        float[] bufferColor = new float[4 * entities.size()];
        for (int i = 0; i < entities.size(); i++) {
            Entity entity = entities.get(i);
            fillBuffer(entity, bufferPos, bufferColor, i * 4);
        }

        floatBufferPos.clear();
        floatBufferPos.put(bufferPos);
        floatBufferPos.flip();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, this.bufferPos);
        GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, floatBufferPos);

        floatBufferColor.clear();
        floatBufferColor.put(bufferColor);
        floatBufferColor.flip();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, this.bufferColor);
        GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, floatBufferColor);
    }

    private void fillBuffer(Entity entity, float[] bufferPos, float[] bufferColor, int i) {
        // First vec4 posRotScale
        /*X: */bufferPos[i + 0] = entity.getPosition().x;
        /*Y: */bufferPos[i + 1] = entity.getPosition().y;

        /*Z: */bufferPos[i + 2] = (float) Math.toRadians(entity.getRotation()); // Convert degrees to radians for easier calculation on GPU
        /*W: */bufferPos[i + 3] = entity.getScale();

        // Second vec4 color
        /*X: */bufferColor[i + 0] = entity.getColor().x;
        /*Y: */bufferColor[i + 1] = entity.getColor().y;
        /*Z: */bufferColor[i + 2] = entity.getColor().z;
        /*W: */bufferColor[i + 3] = entity.getColor().w;
    }
}
