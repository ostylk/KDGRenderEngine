package de.ostylk.kdgrender.render;

import de.ostylk.kdgrender.math.Camera;
import de.ostylk.kdgrender.math.MathUtil;
import de.ostylk.kdgrender.model.Loader;
import de.ostylk.kdgrender.model.Shape;
import de.ostylk.kdgrender.model.entities.Entity;
import de.ostylk.kdgrender.model.primitives.Quad;
import de.ostylk.kdgrender.sfx.PostProcessPipeline;
import de.ostylk.kdgrender.texture.DeferredFramebuffer;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL31;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BatchRenderer {

    private HashMap<Integer, List<Entity>> batchQuads;
    private Shape batchQuad;
    private BatchShader batchShader;
    private BatchBuffer batchBuffer;

    private HashMap<Integer, List<Entity>> meshes;

    private FinalRenderShader finalRenderShader;

    private DeferredFramebuffer firstFramebuffer;
    private DeferredFramebuffer secondFramebuffer;

    private PostProcessPipeline postProcessPipeline;

    private Camera cam;

    public BatchRenderer(Loader loader) {
        defaultViewport();

        batchQuads = new HashMap<>();
        meshes = new HashMap<>();

        batchShader = new BatchShader();
        batchShader.bind();
        batchShader.connectTextureSamplers();   // Connect the texture unit
        batchShader.loadProjectionMatrix(MathUtil.getProjectionMatrix(Display.getWidth(), Display.getHeight()));    // Load the projection matrix
        batchShader.unbind();

        batchBuffer = new BatchBuffer(BatchBuffer.BUFFER_SIZE);

        finalRenderShader = new FinalRenderShader();
        finalRenderShader.bind();
        finalRenderShader.connectTextureUnits();
        finalRenderShader.unbind();

        firstFramebuffer = new DeferredFramebuffer(loader, Display.getWidth(), Display.getHeight());
        secondFramebuffer = new DeferredFramebuffer(loader, Display.getWidth(), Display.getHeight());

        initPrimitives(loader);
    }

    private void initPrimitives(Loader loader) {
        float[] vertices = {
                0.5f,  0.5f,
                -0.5f,  0.5f,
                -0.5f, -0.5f,
                0.5f, -0.5f
        };

        float[] texcoords = {
                1f, 0f,
                0f, 0f,
                0f, 1f,
                1f, 1f
        };

        int[] indices = {
                0, 1, 2,
                0, 2, 3
        };

        batchQuad = loader.getMeshLoader().loadMesh(vertices, texcoords, indices);
        batchQuad.bind();
        batchBuffer.setupBuffer();
        batchQuad.unbind();
    }

    public void submit(Entity entity) {
        if (entity.getMesh() instanceof Quad) {
            // Get the list of quads for the texture
            List<Entity> quads = batchQuads.get(entity.getTexture().getTexID());
            // If it don't exist create it
            if (quads == null) {
                quads = new ArrayList<>();
            }
            // Add the quad to the list
            quads.add(entity);
            // Put it back into the hashmap
            batchQuads.put(entity.getTexture().getTexID(), quads);
        } else {
            // The submitted Mesh isn't a batch primitive
            List<Entity> meshList = meshes.get(entity.getTexture().getTexID());
            if(meshList == null) {
                meshList = new ArrayList<>();
            }
            meshList.add(entity);
            meshes.put(entity.getTexture().getTexID(), meshList);
        }
    }

    public void setCamera(Camera cam) {
        this.cam = cam;
    }
    public void setPostProcessPipeline(PostProcessPipeline pipeline) { this.postProcessPipeline = pipeline; }

    public void clearScreen(Vector3f color) {
        GL11.glClearColor(color.x, color.y, color.z, 1);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
    }

    public void render(Vector3f color) {
        if (Keyboard.isKeyDown(Keyboard.KEY_TAB)) {
            GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
        } else {
            GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
        }

        // First pass: render to framebuffer
        renderPass1(color);
        // Render final result to screen
        renderToScreen(color, processPostProcessPipeline());

        clearLists();
    }

    public void worldRenderPass(Vector3f color) {
        clearScreen(color);

        // Active shader and load needed matrices
        batchShader.bind();
        if (cam != null) {
            batchShader.loadViewMatrix(cam.getViewMatrix());
        } else {
            Matrix4f identity = new Matrix4f();
            identity.setIdentity();
            batchShader.loadViewMatrix(identity);
        }

        // Render all batch quads
        for (int textureID : batchQuads.keySet()) {
            // Bind the texture to be rendered
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID);

            List<Entity> quadEntities = batchQuads.get(textureID);
            batchBuffer.switchBuffer(quadEntities);

            batchQuad.bind();
            GL31.glDrawElementsInstanced(GL11.GL_TRIANGLES, batchQuad.getVertexCount(), GL11.GL_UNSIGNED_INT, 0, quadEntities.size());
            batchQuad.unbind();
        }

        // unbind stuff and stop rendering to framebuffer
        batchShader.unbind();
    }

    private void renderPass1(Vector3f color) {
        // Rendering to framebuffer and clear the framebuffer
        firstFramebuffer.bind();
        worldRenderPass(color);
        firstFramebuffer.unbind(this);
    }

    private DeferredFramebuffer processPostProcessPipeline() {
        if (postProcessPipeline != null) {
            return postProcessPipeline.execute(this, firstFramebuffer, secondFramebuffer);
        }
        return null;
    }

    private void renderToScreen(Vector3f color, DeferredFramebuffer readFramebuffer) {
        if (readFramebuffer == null) readFramebuffer = firstFramebuffer;
        // Clear the screen's content
        clearScreen(color);

        finalRenderShader.bind();

        // Bind the texture the first stage has rendered to and active shader for second pass
        readFramebuffer.bindTextures();

        // Create screen filling quad
        Quad quad = new Quad();
        quad.bind();

        // Draw quad with textures
        GL11.glDrawElements(GL11.GL_TRIANGLES, quad.getVertexCount(), GL11.GL_UNSIGNED_INT, 0);

        quad.unbind();

        readFramebuffer.unbindTextures();
        finalRenderShader.unbind();
    }

    private void clearLists() {
        // Clear the lists but don't delete it because of performance reasons(deleting causes fragmentation)
        batchQuads.values().forEach(List<Entity>::clear);
        meshes.values().forEach(List<Entity>::clear);
    }

    public void defaultViewport() {
        GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
    }
}
