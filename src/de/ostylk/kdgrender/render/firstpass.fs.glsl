#version 330 core

layout (location = 0) out vec4 colorOut;
layout (location = 1) out vec4 posOut;

in VS_OUT {
    vec3 position;
    vec2 texcoord;
    vec4 color;
} fs_in;

uniform sampler2D textureSampler;

void main() {
    // Fetch pixels from texture
    vec4 textureColor = texture(textureSampler, fs_in.texcoord);

    // Multiply by the color
    colorOut = textureColor * fs_in.color;
    posOut = vec4(fs_in.position, 1);
}
