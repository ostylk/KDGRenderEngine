#version 330 core

layout (location = 0) out vec4 color;

in VS_OUT {
    vec2 texcoord;
} fs_in;

uniform sampler2D textureSampler;

void main() {
    color = texture(textureSampler, fs_in.texcoord);
}
