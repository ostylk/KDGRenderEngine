package de.ostylk.kdgrender.render;

import de.ostylk.kdgrender.shader.Shader;
import org.lwjgl.util.vector.Matrix4f;

public class BatchShader extends Shader {

    private static final String VERT_PATH = "firstpass.vs.glsl";
    private static final String FRAG_PATH = "firstpass.fs.glsl";

    private int location_viewMatrix;
    private int location_projectionMatrix;

    private int location_textureSampler;

    public BatchShader() {
        super(BatchShader.class.getResourceAsStream(VERT_PATH), BatchShader.class.getResourceAsStream(FRAG_PATH));
    }

    @Override
    public void initUniformLocations() {
        location_viewMatrix = getUniformLocation("viewMatrix");
        location_projectionMatrix = getUniformLocation("projectionMatrix");

        location_textureSampler = getUniformLocation("textureSampler");
    }

    @Override
    public void initAttributes() {
        bindAttribute("vertex", 0);
        bindAttribute("texcoord", 1);

        bindAttribute("posRotScale", 2);
        bindAttribute("color", 3);
    }

    public void loadViewMatrix(Matrix4f view) { loadMatrix4(location_viewMatrix, view); }
    public void loadProjectionMatrix(Matrix4f proj) {
        loadMatrix4(location_projectionMatrix, proj);
    }

    public void connectTextureSamplers() {
        loadInt(location_textureSampler, 0);
    }
}
