#version 330 core

layout (location = 0) in vec2 vertex;
layout (location = 1) in vec2 texcoord;

// Per instance attributes
layout (location = 2) in vec4 posRotScale;   // xy: position; z: rotation; w: scale
layout (location = 3) in vec4 color;         // xyzw: color (rgba)

out VS_OUT {
    vec3 position;
    vec2 texcoord;
    vec4 color;
} vs_out;

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

void main() {
    vec4 PS = vec4(vertex * posRotScale.w, 0, 1);   // Scale the quad

    float thetaSin = sin(posRotScale.z);
    float thetaCos = cos(posRotScale.z);
    vec2 PR = vec2(0, 0);
    PR.x = thetaCos * PS.x + (-thetaSin) * PS.y;
    PR.y = thetaSin * PS.x +   thetaCos  * PS.y;

    vec4 PT = vec4(PR.xy + posRotScale.xy, 0, 1);   // Transform the quad

    gl_Position = projectionMatrix * viewMatrix * PT;    // Transform it

    // Pass data to fragment shader
    vs_out.position = PT.xyz;
    vs_out.texcoord = texcoord;
    vs_out.color = color;
}
