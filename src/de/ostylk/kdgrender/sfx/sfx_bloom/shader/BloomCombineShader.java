package de.ostylk.kdgrender.sfx.sfx_bloom.shader;

import de.ostylk.kdgrender.shader.Shader;

public class BloomCombineShader extends Shader {

    private int location_textureSampler;
    private int location_bloomTextureSampler;

    private int location_bloomIntensity;

    public BloomCombineShader() {
        super(BloomCombineShader.class.getResourceAsStream("bloomcombineshader.vs.glsl"), BloomCombineShader.class.getResourceAsStream("bloomcombineshader.fs.glsl"));
    }

    @Override
    public void initUniformLocations() {
        location_textureSampler = getUniformLocation("textureSampler");
        location_bloomTextureSampler = getUniformLocation("bloomTextureSampler");

        location_bloomIntensity = getUniformLocation("bloomIntensity");
    }

    public void connectTextureUnits() {
        super.loadInt(location_textureSampler, 0);
        super.loadInt(location_bloomTextureSampler, 1);
    }

    public void loadIntensity(float intensity) {
        super.loadFloat(location_bloomIntensity, intensity);
    }

    @Override
    public void initAttributes() {
        bindAttribute("vertex", 0);
        bindAttribute("texcoord", 1);
    }
}
