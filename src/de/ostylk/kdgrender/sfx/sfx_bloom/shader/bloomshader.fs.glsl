#version 330 core

// Output to bloomFramebuffer
layout (location = 0) out vec4 color;

in VS_OUT {
    vec2 texcoord;
} fs_in;

// Input from readFramebuffer
uniform sampler2D textureSampler;

uniform float bloomThreshold;

void main() {
    vec3 textureColor = texture(textureSampler, fs_in.texcoord).rgb;
    if (length(textureColor) < bloomThreshold) {
        color = vec4(0, 0, 0, 1);
    } else {
        color = vec4(textureColor, 1);
    }
}
