#version 330 core
#extension GL_ARB_explicit_uniform_location : enable

// Output to writeFramebuffer
layout (location = 0) out vec4 color;

in VS_OUT {
    vec2 texcoord;
} fs_in;

//Input from readFramebuffer
layout (location = 0) uniform sampler2D textureSampler;
//Input from bloomFramebuffer
layout (location = 1) uniform sampler2D bloomTextureSampler;

uniform float bloomIntensity;

void main() {
    vec4 textureColor = texture(textureSampler, fs_in.texcoord);
    vec4 bloomColor = texture(bloomTextureSampler, fs_in.texcoord);

    color = textureColor + bloomColor * bloomIntensity;
}
