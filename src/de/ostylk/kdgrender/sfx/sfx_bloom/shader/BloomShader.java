package de.ostylk.kdgrender.sfx.sfx_bloom.shader;

import de.ostylk.kdgrender.shader.Shader;

public class BloomShader extends Shader {

    private int location_textureSampler;
    private int location_bloomThreshold;

    public BloomShader() {
        super(BloomShader.class.getResourceAsStream("bloomshader.vs.glsl"), BloomShader.class.getResourceAsStream("bloomshader.fs.glsl"));
    }

    @Override
    public void initUniformLocations() {
        location_textureSampler = getUniformLocation("textureSampler");
        location_bloomThreshold = getUniformLocation("bloomThreshold");
    }

    public void connectTextureUnits() {
        super.loadInt(location_textureSampler, 0);
    }

    public void loadThreshold(float threshold) {
        super.loadFloat(location_bloomThreshold, threshold);
    }

    @Override
    public void initAttributes() {
        bindAttribute("vertex", 0);
        bindAttribute("texcoord", 1);
    }
}
