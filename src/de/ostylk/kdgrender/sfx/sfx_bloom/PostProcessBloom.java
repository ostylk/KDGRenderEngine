package de.ostylk.kdgrender.sfx.sfx_bloom;

import de.ostylk.kdgrender.model.Loader;
import de.ostylk.kdgrender.model.primitives.Quad;
import de.ostylk.kdgrender.render.BatchRenderer;
import de.ostylk.kdgrender.sfx.PostProcess;
import de.ostylk.kdgrender.sfx.sfx_bloom.framebuffer.BloomFramebuffer;
import de.ostylk.kdgrender.sfx.sfx_bloom.shader.BloomCombineShader;
import de.ostylk.kdgrender.sfx.sfx_bloom.shader.BloomShader;
import de.ostylk.kdgrender.texture.DeferredFramebuffer;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

public class PostProcessBloom extends PostProcess {

    private BloomShader bloomShader;
    private BloomCombineShader bloomCombineShader;

    private BloomFramebuffer bloomFramebuffer;

    private float bloomThreshold = 1.1f;
    private float bloomIntensity = 0.8f;

    public PostProcessBloom(Loader loader, float bloomScale) {
        bloomShader = new BloomShader();
        bloomShader.bind();
        bloomShader.connectTextureUnits();
        bloomShader.unbind();

        bloomCombineShader = new BloomCombineShader();
        bloomCombineShader.bind();
        bloomCombineShader.connectTextureUnits();
        bloomCombineShader.unbind();

        bloomFramebuffer = new BloomFramebuffer(loader, Display.getWidth(), Display.getHeight(), 1 / bloomScale);
    }

    public void setBloomThreshold(float bloomThreshold) { this.bloomThreshold = bloomThreshold; }
    public void setBloomIntensity(float bloomIntensity) { this.bloomIntensity = bloomIntensity; }

    @Override
    public void execute(BatchRenderer renderer, DeferredFramebuffer readFramebuffer, DeferredFramebuffer writeFramebuffer) {
        bloomFramebuffer.bind();
        readFramebuffer.bindTextures();

        bloomShader.bind();
        bloomShader.loadThreshold(bloomThreshold);
        {
            Quad quad = new Quad();
            quad.bind();
            renderer.clearScreen(new Vector3f(0, 0, 0));
            GL11.glDrawElements(GL11.GL_TRIANGLES, quad.getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
            quad.unbind();
        }
        bloomShader.unbind();

        readFramebuffer.unbindTextures();
        bloomFramebuffer.unbind(renderer);

        // second pass

        writeFramebuffer.bind();
        readFramebuffer.bindTexture(0);
        bloomFramebuffer.bindTexture(1);

        bloomCombineShader.bind();
        bloomCombineShader.loadIntensity(bloomIntensity);
        {
            Quad quad = new Quad();
            quad.bind();
            renderer.clearScreen(new Vector3f(0, 0, 0));
            GL11.glDrawElements(GL11.GL_TRIANGLES, quad.getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
            quad.unbind();
        }
        bloomCombineShader.unbind();

        bloomFramebuffer.unbindTexture();
        readFramebuffer.unbindTextures();
        writeFramebuffer.unbind(renderer);
    }
}
