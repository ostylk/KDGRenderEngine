package de.ostylk.kdgrender.sfx.sfx_bloom.framebuffer;

import de.ostylk.kdgrender.model.Loader;
import de.ostylk.kdgrender.texture.Framebuffer;
import de.ostylk.kdgrender.texture.Texture;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import java.nio.IntBuffer;

public class BloomFramebuffer extends Framebuffer {

    private Texture colorTexture;

    public BloomFramebuffer(Loader loader, int width, int height, float inverseBloomScale) {
        super(loader, (int) (width * inverseBloomScale), (int) (height * inverseBloomScale));
    }

    @Override
    public void setup(Loader loader) {
        colorTexture = loader.getTextureLoader().createTexture(getWidth(), getHeight(), GL11.GL_RGB, GL30.GL_RGB32F, GL11.GL_LINEAR);
    }

    @Override
    public void attachTextures() {
        GL30.glFramebufferTexture2D(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT0, GL11.GL_TEXTURE_2D, colorTexture.getTexID(), 0);
    }

    @Override
    public void setupDrawbuffers() {
        IntBuffer drawBuffer = BufferUtils.createIntBuffer(1);
        int draws[] = { GL30.GL_COLOR_ATTACHMENT0 };
        drawBuffer.put(draws);
        drawBuffer.flip();
        GL20.glDrawBuffers(drawBuffer);
    }

    public void bindTexture(int n) {
        GL13.glActiveTexture(GL13.GL_TEXTURE0 + n);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, colorTexture.getTexID());

        GL13.glActiveTexture(GL13.GL_TEXTURE0);
    }

    public void unbindTexture() {
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
    }
}
