package de.ostylk.kdgrender.sfx;

import de.ostylk.kdgrender.render.BatchRenderer;
import de.ostylk.kdgrender.texture.DeferredFramebuffer;

import java.util.Iterator;
import java.util.LinkedList;

public class PostProcessPipeline {

    private LinkedList<PostProcess> pipeline;

    public PostProcessPipeline() {
        pipeline = new LinkedList<>();
    }

    public void addLast(PostProcess postProcess) {
        pipeline.addLast(postProcess);
    }

    public void addFirst(PostProcess postProcess) {
        pipeline.addFirst(postProcess);
    }

    public DeferredFramebuffer execute(BatchRenderer renderer, DeferredFramebuffer firstFramebuffer, DeferredFramebuffer secondFramebuffer) {
        Iterator<PostProcess> iterator = pipeline.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            PostProcess postProcess = iterator.next();
            if (n == 0) {
                postProcess.execute(renderer, firstFramebuffer, secondFramebuffer);
            } else {
                postProcess.execute(renderer, secondFramebuffer, firstFramebuffer);
            }
            n ^= 1;
        }

        return n == 0 ? firstFramebuffer : secondFramebuffer;
    }
}
