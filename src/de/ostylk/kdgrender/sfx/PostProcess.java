package de.ostylk.kdgrender.sfx;

import de.ostylk.kdgrender.render.BatchRenderer;
import de.ostylk.kdgrender.texture.DeferredFramebuffer;

public abstract class PostProcess {

    public abstract void execute(BatchRenderer renderer, DeferredFramebuffer readFramebuffer, DeferredFramebuffer writeFramebuffer);
}
