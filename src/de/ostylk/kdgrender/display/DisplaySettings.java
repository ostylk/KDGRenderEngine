package de.ostylk.kdgrender.display;

public class DisplaySettings {

    public int width;
    public int height;
    public String title;
    public boolean fullscreen;

    public boolean vsync;

    public boolean showFPS;

    /**
     * @param width Width of the display in pixels
     * @param height Height of the display in pixels
     */
    public DisplaySettings(int width, int height) {
        this.width = width;
        this.height = height;
    }
}
