package de.ostylk.kdgrender.display;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.PixelFormat;

public class DisplayManager {

    private static DisplaySettings settings;

    private static final double NANO_PER_SEC = 1000000000D;

    private static long last = 0;
    private static long now = 0;
    private static float delta = 0f;

    private static float counter = 0.0f;
    private static int fps = 0, lastFPS = 0;

    public static void initDisplay(DisplaySettings displaySettings) {
        settings = displaySettings;

        ContextAttribs attribs = new ContextAttribs(3, 3)
                .withProfileCore(true)
                .withForwardCompatible(true);

        try {
            Display.setDisplayMode(new DisplayMode(settings.width, settings.height));
            Display.setFullscreen(settings.fullscreen);
            Display.setVSyncEnabled(settings.vsync);
            Display.setTitle(settings.title);
            Display.create(new PixelFormat(), attribs);
        } catch (LWJGLException e) {
            e.printStackTrace();
        }

        now = System.nanoTime();
    }

    public static void updateDisplay() {
        last = now;
        now = System.nanoTime();
        delta = (float) ((double) (now - last) / NANO_PER_SEC);   // Calculate time difference between frames
        counter += delta;
        fps++;
        if (counter >= 1.0f) {  // If 1 second passed, reset the fps counter and print it out
            counter -= 1.0f;
            updateDisplayTitle(fps);
            lastFPS = fps;
            fps = 0;
        }
        Display.update();
    }

    private static void updateDisplayTitle(int fps) {
        if (!settings.showFPS) return;

        Display.setTitle(settings.title + " | FPS: " + fps);
    }

    public static void closeDisplay() {
        Display.destroy();
    }

    public static float getDelta() { return delta; }
    public static int getFPS() { return lastFPS; }
}
